const path = require(`path`)
const { slash } = require(`gatsby-core-utils`)

exports.createPages = async ({ actions, graphql }) => {
  const { data } = await graphql(`
{
      allWpInvestment {
        nodes {
          id
          date
          title
          uri
          slug
          investment {
            infrastruktura
            lokalizacja
            oddanieInwestycji
            pobytWlascicielski
            powierzchnia
            rejon
            cena
            dlugoscUmowy
            stopaZwrotu
            images {
              sourceUrl
              localFile {
                childImageSharp {
                  fixed(height: 250, width: 250) {
                    width
                    height
                    src
                  }
                }
              }
            }
          }
          seo {
            opengraphUrl
            opengraphTitle
            opengraphType
            schema {
              raw
            }
          }
        }
      }
    }
  `)


  const investments = data.allWpInvestment.nodes
  const investmentsPerPage = 10
  const numberOfPages = Math.ceil(investments.length / investmentsPerPage)
  const allInvestmentsTemplate = require.resolve(`./src/templates/allinvestments.js`)

  Array.from({length: numberOfPages}).forEach((page, index) => {
    actions.createPage({
      component: slash(allInvestmentsTemplate),
      path: index === 0 ? '/investments' : `/investments/${index + 1}`,
      context: {
        investments: investments.slice(index * investmentsPerPage, (index * investmentsPerPage) + investmentsPerPage),
        numberOfPages,
        currentPage: index + 1
      }
    })
  })

  const postTemplate = require.resolve(`./src/templates/investment.js`)
  data.allWpInvestment.nodes.forEach((node, index) => {
    actions.createPage({
      path: `${node.uri}`,
      component: slash(postTemplate),
      context: node,
      id: node.id,
      image: node.investment.images.sourceUrl,
    })
  })
}
