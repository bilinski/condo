## Custom Components & Templates

1. Checkbox
2. ContactForm
3. Input
4. Post Template - source content from WP Rest API, blog post example

## Integrations

1. Contact Forms - ContactForm component push data via Axios to WP Contact Form 7, important to change form ID and input names should be the same as in the WP form settings
2. Wordpress CMS - Additional plugins installed fom Graphql additional data: WPGraphQL SEO, GraphQL Polylang, WPGraphQL for Advanced Custom Fields
