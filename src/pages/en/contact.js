import "../../assets/styles/pages/contact.scss"

import React from "react"

import Layout from "../../components/layout"
import SEO from "../../components/seo"

import ContactForm from "../../components/ContactForm/ContactForm"

const Contact = () => {
  const lang = "en"

  return (
    <Layout lang={lang} translation="/kontakt/">
      <SEO title="Contact" />

      <ContactForm lang={lang} />
    </Layout>
  )
}

export default Contact
