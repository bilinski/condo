import "../../assets/styles/pages/home.scss"

import React from "react"

import Layout from "../../components/layout"
import SEO from "../../components/seo"

const Index = () => {
  const lang = "en"

  return (
    <Layout lang={lang} translation="/">
      <SEO title="Home" />
    </Layout>
  )
}

export default Index
