import "../assets/styles/pages/contact.scss"

import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"

import ContactForm from "../components/ContactForm/ContactForm"

const Contact = () => {
  const lang = "pl"

  return (
    <Layout lang={lang} translation="/en/contact/">
      <SEO title="Kontakt" />

      <ContactForm lang={lang} />
    </Layout>
  )
}

export default Contact
