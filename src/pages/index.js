import "../assets/styles/pages/home.scss"
import React from "react"
import Herosection from "../components/Herosection/Herosection"
import Button from "../components/Button/Button"
import Layout from "../components/layout"
import Slider from "../components/Slider/ResponsiveSlider"
import Newsslider from "../components/Slider/ResponsiveSlider"
import CondoHotelsList from "../components/Homepage/CondoHotelsList"
import SEO from "../components/seo"
import { Grid, Container, Box } from "@material-ui/core"
import city from "../assets/images/homepage/city.png"
import lake from "../assets/images/homepage/lake.png"
import mountains from "../assets/images/homepage/mountains.png"
import sea from "../assets/images/homepage/sea.png"
import ico1 from "../assets/images/homepage/ico1.png"
import ico2 from "../assets/images/homepage/ico2.png"
import ico3 from "../assets/images/homepage/ico3.png"
import homephoto3 from "../assets/images/homepage/homephoto3.png"
import homephoto2 from "../assets/images/homepage/homephoto2.png"
import homephoto1 from "../assets/images/homepage/homephoto1.png"
import osadamorska from "../assets/images/homepage/osadamorska.png"
import osadaspa from "../assets/images/homepage/osadaspa.png"
import piemontehotel from "../assets/images/homepage/piemontehotel.png"
import belmontehotel from "../assets/images/homepage/belmontehotel.png"


const Index = pageContext => {
  return (
    <Layout>
      <SEO title="Strona główna" />
      <Herosection />
      <Container maxWidth="false" className="autoheight width80">
        <div className="sectionwrapper">
          <h2 className="h1 center">Condo hotele</h2>
          <h2 className="h2subtitle">
            Atrakcyjne apartamenty inwestycyjne na sprzedaż
          </h2>
        </div>
        <div className="flexrow">
          <CondoHotelsList />
        </div>
        {/*  <Slider /> */}
        <div className="flexrow autoheight">
          <Button label="Lista inwestycji" />
        </div>
      </Container>
      <Container maxWidth="lg" className="autoheight">
        <div className="flexcolumn autoheight">
          <div className="flexrow">
            <div className="imgpadding">
            <div id="Nad_morzem">
				      <span>Nad morzem</span>
			        </div>
              <img className="img-fluid" src={sea} />
            </div>
            <div className="imgpadding">
            <div id="W_gorach">
				      <span>W górach</span>
			        </div>
              <img className="img-fluid" src={mountains} />
            </div>
          </div>
          <div className="flexrow">
            <div className="imgpadding">
            <div id="Nad_jeziorem">
				      <span>W górach</span>
			        </div>
              <img className="img-fluid" src={lake} />
            </div>
            <div className="imgpadding">
            <div id="W_miescie">
				      <span>W górach</span>
			        </div>
              <img className="img-fluid" src={city} />
            </div>
          </div>
        </div>
      </Container>
      <h2 className="h1 center">Popularne Inwestycje</h2>
      <Container maxWidth="lg" className="sliderheight">
        <Grid container direction="row" justify="center" alignItems="center">
          <div className="flexcolumn autoheight">
            <div className="flexrow">
              <div className="imgpadding">
                <img className="img-fluid" src={osadamorska} />
                <p className="phototags">Osada Morska Resort & Spa Łeba</p>
              </div>
              <div className="imgpadding">
                <img className="img-fluid" src={piemontehotel} />
                <p className="phototags">Piemonte Hotel & Resort</p>
              </div>
            </div>
            <div className="flexrow">
              <div className="imgpadding">
                <img className="img-fluid" src={osadaspa} />
                <p className="phototags">Osada Morska Resort & Spa</p>
              </div>
              <div className="imgpadding">
                <img className="img-fluid" src={belmontehotel} />
                <p className="phototags">Belmonte Hotel & Resort</p>
              </div>
            </div>
          </div>
          {/*           <Newsslider /> */}
        </Grid>
      </Container>
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="center"
        className="sectionwrapper"
      >
        <Grid container direction="row" justify="center" alignItems="center">
          <Grid item xs={12} lg={4}>
            <div className="marginright">
              <h2 className="h1 headingpadding">Jesteśmy ekspertami</h2>
              <h2 className="h2subtitle headingpadding left">
                na rynku nieruchomości inwestycyjnych
              </h2>
              <p>
                Od wielu lat pomagamy Polakom spełnić marzenia o zarabianiu na
                nieruchomościach. Nasz zespół tworzą specjaliści z wieloletnim
                doświadczeniem w branży hotelarskiej i finansowej. Zdobytą
                wiedzę wykorzystujemy, by efektywnie pomagać inwestorom w
                znalezieniu idealnego lokalu pod wynajem. Dysponujemy bazą
                apartamentów inwestycyjnych w rekreacyjnych kurortach nad
                morzem, w górach, nad jeziorem oraz w mieście.
              </p>
              <br />
              <p>
                Wybieramy inwestycje na najwyższym poziomie, z wszelkimi
                udogodnieniami i wykończeniem na wysokim standardzie. Oferowane
                hotele i lokale inwestycyjne zlokalizowane są w najciekawszych
                obszarach rekreacyjnych w takich miastach jak: Kołobrzeg, Łeba,
                Krynica Zdrój, Karpacz, Zakopane czy Bukowina.
              </p>
              <br />
              <p>
                Jeśli interesuje Cię inwestowanie w pokoje hotelowe – sprawdź co
                możemy Ci zaoferować.
              </p>
              <div className="flexrow autoheight">
                <Button label="O nas" />
              </div>
            </div>
          </Grid>
          <div className="flexcolumn">
            <img className="img-fluid imgpadding" src={homephoto1} />
            <img className="img-fluid imgpadding" src={homephoto2} />
          </div>
        </Grid>
      </Grid>
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="center"
        className="sectionwrapper"
      >
        <Grid container direction="row" justify="center" alignItems="center">
          <img className="img-fluid" src={homephoto3} />
          <Grid item xs={12} lg={4}>
            <div className="marginleft">
              <h2 className="h1 headingpadding">
                Do kogo skierowana jest nasza oferta?
              </h2>
              <p>
                Apartamenty i pokoje pod wynajem to rozwiązanie dedykowane
                osobom, które chcą zainwestować własne środki w zakup
                nieruchomości, by czerpać zysk z ich wynajmu. Apartamenty mogą
                nabyć zarówno klienci indywidualni jak i firmy, finansując zakup
                ze środków w gotówce lub pochodzących z kredytu.
              </p>
              <br />
              <p>
                Na zakup pokoju hotelowego w prestiżowej lokalizacji decyduje
                się coraz więcej osób, które chcą łączyć posiadanie luksusowego
                apartamentu na wynajem, z możliwością dysponowania nim na użytek
                własny, przez określony czas. Jeśli chcesz do nich dołączyć –
                przejdź do zakładki Lista inwestycji i sprawdź aktualną ofertę
                na apartamenty inwestycyjne nad morzem, w górach i w mieście.
              </p>
              <div className="flexrow autoheight">
                <Button label="Lista inwestycji" />
              </div>
            </div>
          </Grid>
        </Grid>
      </Grid>
      <Container maxWidth="lg" className="sectionwrapper">
        <h2 className="h1 sectionwrapper center">Jak to działa?</h2>
        <div className="flexrowtopalign">
          <div className="width30">
            <img className="img-fluid" src={ico1} />
            <br />
            Kupując apartament w wybudowanym przez dewelopera obiekcie, inwestor
            staje się jego pełnoprawnym właścicielem zapisanym w księdze
            wieczystej. Każdy nabyty lokal posiada również umowę najmu. Zgodnie
            z nią, operator hotelowy gwarantuje wynajmowanie zakupionego
            apartamentu i wypłatę zysku właścicielowi. W zależności od obiektu
            okres najmu może być różny – zwykle wynosi 10-15 lat, z możliwością
            przedłużenia.
          </div>
          <div className="width30">
            <img className="img-fluid" src={ico2} />
            <br />
            Formalnie wynajmem zajmuje się operator hotelowy. To duża wygoda,
            zwłaszcza dla inwestorów, którzy nie mają czasu by samodzielnie
            zająć się szukaniem najemców i podnajmem. Jest to również komfortowe
            z punktu widzenia gości hotelowych, którzy po prostu rezerwują
            apartament, bez wchodzenia w szczegóły kto jest jego właścicielem.
          </div>
          <div className="width30">
            <img className="img-fluid" src={ico3} />
            <br />
            Wynajmujący mogą korzystać z szeregu usług dodatkowych jak recepcja,
            restauracja, strefy relaksu czy przestrzeń konferencyjna. Dla
            klientów hotelu obiekty w systemie condo nie różnią się niczym od
            „zwykłych” hoteli.
          </div>
        </div>
      </Container>
    </Layout>
  )
}

export default Index
