import "./footer.scss"
import { Link } from "gatsby"
import styled from "styled-components"
import { Grid, Box, Container } from "@material-ui/core"

import React from "react"

const FooterItem = styled(Link)`
  text-decoration: none;
  color: #fff;
  margin: 0;
  transition: all 200ms ease-in;
  position: relative;

  :hover {
    color: goldenrod;
    ::after {
      width: 100%;
    }
  }
`

const FooterSubItem = styled(Link)`
  text-decoration: none;
  font-size: 10px;
  color: #fff;
  margin: 0;
  padding: 5px;
  transition: all 200ms ease-in;
  position: relative;

  :hover {
    color: #fff;
    ::after {
      width: 100%;
    }
  }
`

const Footer = () => {
  return (
    <footer className="footer">
      <div className="footermenu">
        <div className="submenu">
          <div className="menucolumn">
            <p className="bold">Menu</p>
            <br />
            <FooterItem to="/">Strona Główna</FooterItem>
            <br />
            <FooterItem to="/">Lista Inwestycji</FooterItem>
            <br />
            <FooterItem to="/">Mapa Inwestycji</FooterItem>
            <br />
            <FooterItem to="/">Rynek Wtórny</FooterItem>
            <br />
          </div>
          <div className="menucolumn">
            <p className="bold">Informacje</p>
            <br />
            <FooterItem to="/">Artykuły</FooterItem>
            <br />
            <FooterItem to="/">O nas</FooterItem>
            <br />
            <FooterItem to="/">Kontakt</FooterItem>
            <br />
          </div>
          <div className="menucolumn">
            <p className="bold">Obsługa Klienta</p>
            <br />
            <FooterItem to="/">Jak to działa?</FooterItem>
            <br />
            <FooterItem to="/">FAQ</FooterItem>
            <br />
            <FooterItem to="/">Covid-19</FooterItem>
            <br />
            <FooterItem to="/">Bezpieczeństwo</FooterItem>
            <br />
          </div>
        </div>
        <div className="menucolumn">
          <p className="bold">Dane Kontaktowe:</p>
          <br />
          <p>ul. Wioślarska 8</p>
          <p>00-411 Warszawa</p>
          <p>+48 574 000 875</p>
        </div>
      </div>
      <div className="submenuwrapper">
      <div className="footersubmenu">
          <div>
            <FooterSubItem to="/">2021 Rynek Condo</FooterSubItem>
          </div>
          <div>
            <FooterSubItem to="/">Regulamin</FooterSubItem>
            <FooterSubItem to="/">|</FooterSubItem>
            <FooterSubItem to="/">Polityka Prywatności</FooterSubItem>
          </div>
          <div>
            <FooterSubItem to="/">Projekt i wykonanie: ADREAM</FooterSubItem>
          </div>
      </div>
      </div>
      </footer>
  )
}
export default Footer
