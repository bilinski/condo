import "./input.scss"

import React from "react"

const Input = ({
  label,
  id,
  type,
  name,
  value,
  disabled,
  onChange,
  placeholder,
  required,
}) => {
  return (
    <div className="input">
      {label && (
        <label className="input__label" htmlFor={name}>
          {label}
          {required && <sup>*</sup>}
        </label>
      )}
      <input
        className="input__input"
        type={type}
        id={id}
        name={name}
        value={value}
        onChange={onChange}
        disabled={disabled}
        placeholder={placeholder}
        required={required}
      />
    </div>
  )
}

export default Input
