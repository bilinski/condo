import React from "react"
import "../Button/button.scss"

const buttonStyle = {
  margin: "10px 0",
}

const Button = ({ label, handleClick }) => (
  <button className="button" style={buttonStyle} onClick={handleClick}>
    {label}
  </button>
)

export default Button
