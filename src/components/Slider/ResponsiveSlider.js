import React, { Component } from "react"
import Slider from "react-slick"
import "slick-carousel/slick/slick.css"
import "slick-carousel/slick/slick-theme.css"
import { StaticQuery, graphql } from "gatsby"
import { Container } from "@material-ui/core"

class ResponsiveSlider extends Component {
  render() {
    var settings = {
      dots: true,
      infinite: false,
      speed: 500,
      slidesToShow: 4,
      slidesToScroll: 4,
      initialSlide: 0,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true,
          },
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            initialSlide: 2,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
    }
    console.log(this.props)
    return (
      <Container maxWidth="lg">
        <div>
          <div>
          <p>saadfsdfasdf</p>
          </div>

          <Slider {...settings}>
            <div>
              <p>Tytuł: {this.props.site.nodes[0].title}</p>
              <p>Cena: {this.props.site.nodes[0].investment.cena}</p>
              <p>
                Długość umowy:{" "}
                {this.props.site.nodes[0].investment.dlugoscUmowy}
              </p>
              <p>
                Oddanie inwestycji:{" "}
                {this.props.site.nodes[0].investment.oddanieInwestycji}
              </p>
              <p>
                Stopa Zwrotu: {this.props.site.nodes[0].investment.stopaZwrotu}
              </p>
              <p>
                Powierzchnia: {this.props.site.nodes[0].investment.powierzchnia}
              </p>
              <p>
                Lokalizacja: {this.props.site.nodes[0].investment.lokalizacja}
              </p>
            </div>
            <div>
              <p>Tytuł: {this.props.site.nodes[1].title}</p>
              <p>Cena: {this.props.site.nodes[1].investment.cena}</p>
              <p>
                Długość umowy:{" "}
                {this.props.site.nodes[1].investment.dlugoscUmowy}
              </p>
              <p>
                Oddanie inwestycji:{" "}
                {this.props.site.nodes[1].investment.oddanieInwestycji}
              </p>
              <p>
                Stopa Zwrotu: {this.props.site.nodes[1].investment.stopaZwrotu}
              </p>
              <p>
                Powierzchnia: {this.props.site.nodes[1].investment.powierzchnia}
              </p>
              <p>
                Lokalizacja: {this.props.site.nodes[1].investment.lokalizacja}
              </p>
            </div>
            <div>
              <p>Tytuł: {this.props.site.nodes[0].title}</p>
              <p>Cena: {this.props.site.nodes[0].investment.cena}</p>
              <p>
                Długość umowy:{" "}
                {this.props.site.nodes[0].investment.dlugoscUmowy}
              </p>
              <p>
                Oddanie inwestycji:{" "}
                {this.props.site.nodes[0].investment.oddanieInwestycji}
              </p>
              <p>
                Stopa Zwrotu: {this.props.site.nodes[0].investment.stopaZwrotu}
              </p>
              <p>
                Powierzchnia: {this.props.site.nodes[0].investment.powierzchnia}
              </p>
              <p>
                Lokalizacja: {this.props.site.nodes[0].investment.lokalizacja}
              </p>
            </div>
            <div>
              <p>Tytuł: {this.props.site.nodes[0].title}</p>
              <p>Cena: {this.props.site.nodes[0].investment.cena}</p>
              <p>
                Długość umowy:{" "}
                {this.props.site.nodes[0].investment.dlugoscUmowy}
              </p>
              <p>
                Oddanie inwestycji:{" "}
                {this.props.site.nodes[0].investment.oddanieInwestycji}
              </p>
              <p>
                Stopa Zwrotu: {this.props.site.nodes[0].investment.stopaZwrotu}
              </p>
              <p>
                Powierzchnia: {this.props.site.nodes[0].investment.powierzchnia}
              </p>
              <p>
                Lokalizacja: {this.props.site.nodes[0].investment.lokalizacja}
              </p>
            </div>
            <div>
              <p>Tytuł: {this.props.site.nodes[0].title}</p>
              <p>Cena: {this.props.site.nodes[0].investment.cena}</p>
              <p>
                Długość umowy:{" "}
                {this.props.site.nodes[0].investment.dlugoscUmowy}
              </p>
              <p>
                Oddanie inwestycji:{" "}
                {this.props.site.nodes[0].investment.oddanieInwestycji}
              </p>
              <p>
                Stopa Zwrotu: {this.props.site.nodes[0].investment.stopaZwrotu}
              </p>
              <p>
                Powierzchnia: {this.props.site.nodes[0].investment.powierzchnia}
              </p>
              <p>
                Lokalizacja: {this.props.site.nodes[0].investment.lokalizacja}
              </p>
            </div>
            <div>
              <h3>6</h3>
            </div>
            <div>
              <h3>7</h3>
            </div>
            <div>
              <h3>8</h3>
            </div>
          </Slider>
        </div>
      </Container>
    )
  }
}

export default () => (
  <StaticQuery
    query={graphql`
      query {
        allWpInvestment {
          nodes {
            investment {
              cena
              dlugoscUmowy
              fieldGroupName
              infrastruktura
              lokalizacja
              pobytWlascicielski
              stopaZwrotu
              rejon
              powierzchnia
              oddanieInwestycji
              images {
                uri
              }
            }
            title
          }
        }
      }
    `}
    render={data => <ResponsiveSlider site={data.allWpInvestment} />}
  />
)
