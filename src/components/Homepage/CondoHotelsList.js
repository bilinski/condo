import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"
import "../Homepage/CondoHotelsList.scss"

const CondoHotelsList = () => {
  const data = useStaticQuery(graphql`
    query {
      allWpInvestment {
        nodes {
          id
          date
          title
          uri
          slug
          investment {
            infrastruktura
            lokalizacja
            oddanieInwestycji
            pobytWlascicielski
            powierzchnia
            rejon
            cena
            dlugoscUmowy
            stopaZwrotu
            images {
              sourceUrl
              localFile {
                childImageSharp {
                  fixed(height: 250, width: 250) {
                    width
                    height
                    src
                  }
                }
              }
            }
          }
          seo {
            opengraphUrl
            opengraphTitle
            opengraphType
            schema {
              raw
            }
          }
        }
      }
    }
  `)

  console.log(data)

  return (
    <>
      {data.allWpInvestment.nodes.map(investment => (
        <div key={investment.id} className="homelistitem">
          <div>
            <Img
              fixed={
                investment.investment.images[0].localFile.childImageSharp.fixed
              }
            />
          </div>
          <div className="">
            <div>
              <p>
                Lokalizacja:{" "}
                <span
                  dangerouslySetInnerHTML={{
                    __html: investment.investment.lokalizacja,
                  }}
                />
              </p>
            </div>
            <div>
              <h1 className="phototags" dangerouslySetInnerHTML={{ __html: investment.title }} />
            </div>
          </div>
        </div>
      ))}
    </>
  )
}

export default CondoHotelsList
