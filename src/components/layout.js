import React from "react"
import Header from "./Header/Header"
import Footer from "./Footer/Footer"
import ContactForm from "../components/ContactForm/ContactForm"

import "../assets/styles/app.scss"

class Layout extends React.Component {
  render() {
    const { children } = this.props
    return (
      <>
        <Header />
        <main>{children}</main>
        <ContactForm />
        <Footer />
      </>
    )
  }
}

export default Layout
