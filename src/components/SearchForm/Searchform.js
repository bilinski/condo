import React from "react"
import "./Searchform.scss"

const Searchform = () => {
  return (
    <div className="formwrapper">
      <form className="form"> 
        <div className="row">
          <div className="col-10">
            <label for="firstname">Koszt Inwestycji</label>
            <input
              type="text"
              id="fname"
              name="firstname"
              placeholder="od.."
              size="10"
            />
          </div>
          <div className="col-10">
            <input
              type="text"
              id="fname"
              name="firstname"
              placeholder="do.."
              size="10"
            />
          </div>
          <div className="col-10">
            <label for="firstname">Region</label>
            <select id="country" name="country">
              <option value="" disabled selected>
                Wybierz Region
              </option>
              <option value="australia">Australia</option>
              <option value="canada">Canada</option>
              <option value="usa">USA</option>
            </select>
          </div>
          <div className="col-10">
            <label for="firstname"></label>
            <input
              type="text"
              id="lname"
              name="lastname"
              placeholder="Nazwa Inwestycji/Lokalizacja"
              size="25"
            />
          </div>
          <div className="col-10">
            <input type="submit" value="Zobacz Oferty" />
          </div>
        </div>
      </form>
    </div>
  )
}

export default Searchform
