import React from "react"
import "./HeroSection.scss"
import Searchform from "../SearchForm/Searchform"
import styled from "styled-components"

const ButtonStyled = styled.button`
  color: #000000;
  background: #ffffff;
  padding: 15px 35px;
  border: 2px;
  border-style: solid;
  border-width: 2px;
  border-radius: 2px;
  font-size: 16px;
  font-weight: 600;
  cursor: pointer;
`

const Herosection = () => {
  return (
    <>
      <div className="hero-container">
        <div className="title1">
          <h1 className="title">Zainwestuj w nieruchomości</h1>
        </div>
        <div className="subtitle1">
          <h2 className="subtitle">
            Znajdź ofertę dopasowaną do Twoich wymagań
          </h2>
        </div>


          <Searchform />

        {/*         <div className="buttons">
          <ButtonStyled>Nad Morzem</ButtonStyled>
          <ButtonStyled>Nad Jeziorem</ButtonStyled>
          <ButtonStyled>W górach</ButtonStyled>
          <ButtonStyled>W mieście</ButtonStyled>
          <ButtonStyled>Zobacz oferty</ButtonStyled>
        </div> */}
      </div>
    </>
  )
}

export default Herosection
