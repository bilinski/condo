import "./contact-form.scss"
import styled from "styled-components"
import React, { useState } from "react"
import axios from "axios"
import logo from "../../assets/images/logo2.png"

import {
  Grid,
  Box,
  Container,
  TextField,
  Checkbox,
  Button,
} from "@material-ui/core"

import t from "../../locale"

const StyledInput = styled(TextField)`
width: 250px;
  }
`

const ContactForm = ({ lang }) => {
  const [send, setSend] = useState("")
  const [name, setName] = useState("")
  const [email, setEmail] = useState("")
  const [phone, setPhone] = useState("")
  const [legalFirst, setLegalFirst] = useState(false)
  const [legalSecond, setLegalSecond] = useState(false)
  const [legalThird, setLegalThird] = useState(false)

  const resetForm = () => {
    setSend(false)
    setName("")
    setEmail("")
    setPhone("")
    setLegalFirst(false)
    setLegalSecond(false)
    setLegalThird(false)
  }

  const formSubmit = e => {
    e.preventDefault()
    setSend(true)

    let formData = new FormData()

    formData.set("name", name)
    formData.set("email", email)
    formData.set("phone", phone)
    formData.set("legalFirst", legalFirst)
    formData.set("legalSecond", legalSecond)
    formData.set("legalThird", legalThird)

    axios({
      method: "post",
      url:
        "http://adream-starter-gatsby-wp.zenx.pl/wp-json/contact-form-7/v1/contact-forms/6/feedback/",
      data: formData,
      headers: { "Content-Type": "multipart/form-data" },
    })
      .then(res => {
        console.log("Submit success")
        resetForm()
      })
      .catch(() => {
        console.log("Submit error")
      })
  }

  return (
    <Container maxWidth="false" className="formcontainer">
      <div className="contact-formleft">
      <img className="img-fluid" src={logo} />
      <p>ul. Wioślarska 8</p>
      <p>00-411 Warszawa</p>
      <p>+48 574 000 875</p>
      </div>
      <div className="form-wrapper">
        <form onSubmit={formSubmit}>
          <Grid className="contact-form">
            <StyledInput
              label={t("Imię i nazwisko", lang)}
              type="text"
              id="name"
              value={name}
              onChange={e => setName(e.target.value)}
              variant="outlined"
              className="contact-form-element"
            />
            <StyledInput
              label={t("E-mail", lang)}
              type="email"
              id="email"
              value={email}
              onChange={e => setEmail(e.target.value)}
              required
              variant="outlined"
              s
            />
            <StyledInput
              label={t("Phone", lang)}
              type="tel"
              id="phone"
              name="phone"
              value={phone}
              onChange={e => setPhone(e.target.value)}
              required
              variant="outlined"
            />
          </Grid>
          <div className="agreement-wrapper">
            <div className="agreement-wrapper-first">
              <Grid
                container
                item
                direction="row"
                justify="flex-start"
                alignItems="center"
                spacing={0}
                wrap="nowrap"
              >
                <Grid item>
                  <Box>
                    <Checkbox
                      label={t("Zgoda pierwsza", lang)}
                      name="legalFirst"
                      checked={legalFirst}
                      onChange={e => setLegalFirst(!legalFirst)}
                      required
                    />
                  </Box>
                </Grid>
                <Box>
                  <Grid item>
                    <p className="contact-form-agreements">
                      Wyrażam zgodę na przetwarzanie przez Oak Property Sp. z
                      o.o. z siedzibą w Warszawie (00-411), ul. Wioślarska 8,
                      moich danych osobowych zawartych w niniejszym formularzu
                      kontaktowym w celu i zakresie koniecznym do realizacji
                      zgłoszenia.
                    </p>
                  </Grid>
                </Box>
              </Grid>
              <Grid
                item
                container
                direction="row"
                justify="flex-start"
                alignItems="center"
                spacing={0}
                wrap="nowrap"
              >
                <Grid item>
                  <Box>
                    <Checkbox
                      label={t("Zgoda druga", lang)}
                      name="legalSecond"
                      checked={legalSecond}
                      onChange={e => setLegalSecond(!legalSecond)}
                    />
                  </Box>
                </Grid>
                <Box>
                  <Grid item>
                    <p className="contact-form-agreements">
                      Wyrażam zgodę na przetwarzanie przez Oak Property Sp. z
                      o.o. z siedzibą w Warszawie (00-411), ul. Wioślarska 8,
                      moich danych osobowych zawartych w niniejszym formularzu
                      kontaktowym w celu przesyłania mi ofert handlowych drogą
                      elektroniczną.
                    </p>
                  </Grid>
                </Box>
              </Grid>
              <Grid
                item
                container
                direction="row"
                justify="flex-start"
                alignItems="center"
                spacing={0}
                wrap="nowrap"
              >
                <Grid item>
                  <Checkbox
                    label={t("Zgoda trzecia", lang)}
                    name="legalThird"
                    checked={legalThird}
                    onChange={e => setLegalThird(!legalThird)}
                  />
                </Grid>
                <Box>
                  <Grid item>
                    <p className="contact-form-agreements">
                      {" "}
                      Wyrażam zgodę na przetwarzanie przez Oak Property Sp. z
                      o.o. z siedzibą w Warszawie (00-411), ul. Wioślarska 8,
                      moich danych osobowych zawartych w niniejszym formularzu
                      kontaktowym w celu kontaktu telefonicznego ze strony
                      przedstawicieli spółki w sprawach związanych z ofertą
                      handlową.
                    </p>
                  </Grid>
                </Box>
              </Grid>
            </div>
            <div className="agreement-wrapper-second">
              <Grid
                container
                item
                direction="column"
                justify="center"
                alignItems="center"
                spacing={0}
              >
                <Grid item>
                  <Button
                    type="submit"
                    variant="contained"
                    disabled={!legalFirst}
                  >
                    {send === true
                      ? t("Wysyłanie..", lang)
                      : t("Wyślij wiadomość", lang)}
                  </Button>
                </Grid>
              </Grid>
            </div>
          </div>
        </form>
      </div>
    </Container>
  )
}

export default ContactForm
