import React from "react"
import { graphql } from "gatsby"
import "../assets/styles/pages/investment.scss"
import Layout from "../components/layout"
import SEO from "../components/seo"
import { Grid, Container, Box } from "@material-ui/core"
import styled from "styled-components"
import { Link } from "gatsby"
import { GatsbyImage, getImage } from "gatsby-plugin-image"
import Img from "gatsby-image"

const SubNavigation = styled.nav`
  height: 10vh;
  display: flex;
  background-color: #fff;
  position: relative;
  justify-content: space-between;
  text-transform: uppercase;
  border-bottom: 2px solid #33333320;
  margin: 0 auto;
  padding: 0 5vw;
  z-index: 2;
  align-self: center;
`

const Navbox = styled.div`
  display: flex;
  height: 100%;
  justify-content: flex-end;
  align-items: center;
`

const NavItem = styled(Link)`
  text-decoration: none;
  color: #111;
  display: inline-block;
  white-space: nowrap;
  margin: 0 0 1vw;
  padding-right: 20px;
  transition: all 200ms ease-in;
  position: relative;

  :after {
    position: absolute;
    bottom: 0;
    left: 0;
    right: 0;
    width: 0%;
    content: ".";
    color: transparent;
    background: goldenrod;
    height: 1px;
    transition: all 0.4s ease-in;
  }

  :hover {
    color: goldenrod;
    ::after {
      width: 100%;
    }
  }

  @media (max-width: 768px) {
    padding: 20px 0;
    font-size: 1.5rem;
    z-index: 6;
  }
`

function postTemplate({ pageContext, data }) {
  console.log(pageContext)
  const { investment, id, image, title } = pageContext
  console.log(data)

  return (
    <>
      <Layout>
        <Container
          className="sectionheight"
          direction="row"
          justify="center"
          alignItems="center"
        >
          <Grid
            container
            direction="column"
            justify="flex-start"
            alignItems="flex-start"
          >
            <Grid item>
              <Box>
                <h1>{title}</h1>
              </Box>
            </Grid>
            <Grid
              container
              direction="row"
              justify="flex-start"
              alignItems="flex-start"
            >
              <Navbox>
                <NavItem to="/">Opis</NavItem>
                <NavItem to="/investments">Plan Inwestycji</NavItem>
                <NavItem to="/mapofinvestments">Udogodnienia</NavItem>
                <NavItem to="/secondhandmarket">Kalkulator Zysku</NavItem>
                <NavItem to="/services">Apartamenty</NavItem>
                <NavItem to="/404">O inwestorze</NavItem>
                <NavItem to="/aboutus">Lokalizacja</NavItem>
                <NavItem to="/aboutus">Galeria</NavItem>
                <NavItem to="/contact">Kontakt</NavItem>
              </Navbox>
            </Grid>
          </Grid>
        </Container>
        <Container className="investmenthero">
          <Img
            fluid={
              data.allWpInvestment.nodes[0].investment.images[0].localFile
                .childImageSharp.fluid
            }
          />
        </Container>
        <Container
          className="sectionheight"
          direction="row"
          justify="center"
          alignItems="center"
        >
          <h1>cena:{pageContext.investment.rejon}</h1>
          <h1>id{pageContext.id}</h1>
          <br />
          <h1>id{pageContext.uri}</h1>
        </Container>
      </Layout>
    </>
  )
}

export const data = graphql`
  query($id: String!) {
    allWpInvestment(filter: { id: { eq: $id } }) {
      nodes {
        id
        date
        title
        uri
        slug
        investment {
          cena
          dlugoscUmowy
          fieldGroupName
          lokalizacja
          oddanieInwestycji
          pobytWlascicielski
          powierzchnia
          stopaZwrotu
          images {
            sourceUrl
            localFile {
              childImageSharp {
                fluid(maxWidth: 700) {
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
        seo {
          opengraphUrl
          opengraphTitle
          opengraphType
          schema {
            raw
          }
        }
      }
    }
  }
`

export default postTemplate
