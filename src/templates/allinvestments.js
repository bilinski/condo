import React from "react"
import { graphql } from "gatsby"
import "../assets/styles/pages/listofinvestments.scss"
import Layout from "../components/layout"
import SEO from "../components/seo"
import { Grid, Container, Box } from "@material-ui/core"
import styled from "styled-components"
import { Link } from "gatsby"
import { GatsbyImage, getImage } from "gatsby-plugin-image"
import Img from "gatsby-image"

function allInvestmentsTemplate({ pageContext, data }) {
  console.log(pageContext)
  /*   const image = getImage(investment.investment.images[0].localFile) */
  /*   console.log(data) */

  return (
    <>
      <Layout>
        {/*      <GatsbyImage image={pageContext.investments.investment.images[0].localFile} alt={data.blogPost.author} /> */}
        <div className="container">
          {pageContext.investments.map(investment => (
            <>
              <div className="listitemwrapper">
                <div key={investment.id} className="listitem">
                  <div>
                    <Img
                      fixed={
                        investment.investment.images[0].localFile
                          .childImageSharp.fixed
                      }
                    />
                  </div>
                  <div className="itemdatawrapper">
                    <div className="investmenttitle">
                      <h1
                        dangerouslySetInnerHTML={{ __html: investment.title }}
                      />
                    </div>
                    <div className="itemdatarow">
                      <div>
                        <p>
                          Cena: od
                          <span
                            dangerouslySetInnerHTML={{
                              __html: investment.investment.cena,
                            }}
                          />
                        </p>
                        <p>
                          Długość umowy:{" "}
                          <span
                            dangerouslySetInnerHTML={{
                              __html: investment.investment.dlugoscUmowy,
                            }}
                          />
                        </p>
                        <p>
                          Oddanie inwestycji:{" "}
                          <span
                            dangerouslySetInnerHTML={{
                              __html: investment.investment.oddanieInwestycji,
                            }}
                          />
                        </p>
                      </div>
                      <div>
                        <p>
                          Stopa zwrotu:{" "}
                          <span
                            dangerouslySetInnerHTML={{
                              __html: investment.investment.oddanieInwestycji,
                            }}
                          />
                        </p>
                        <p>
                          Powierzchnia:{" "}
                          <span
                            dangerouslySetInnerHTML={{
                              __html: investment.investment.oddanieInwestycji,
                            }}
                          />
                        </p>
                        <p>
                          Lokalizacja:{" "}
                          <span
                            dangerouslySetInnerHTML={{
                              __html: investment.investment.oddanieInwestycji,
                            }}
                          />
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="logoanddetails">
                    <div><p>LOGO</p></div>
                    <button
                       type="button"
                       onClick={(e) => {
                         e.preventDefault();
                         window.location.href=`http://localhost:8000${investment.uri}`
                         }}
                    >Szczegóły</button>
                  </div>
                </div>
              </div>
            </>
          ))}
        </div>
      </Layout>
    </>
  )
}

export const data = graphql`
  query {
    allWpInvestment {
      nodes {
        id
        date
        title
        uri
        slug
        investment {
          infrastruktura
          lokalizacja
          oddanieInwestycji
          pobytWlascicielski
          powierzchnia
          rejon
          cena
          dlugoscUmowy
          stopaZwrotu
          images {
            sourceUrl
            localFile {
              childImageSharp {
                fluid(maxWidth: 700) {
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
        seo {
          opengraphUrl
          opengraphTitle
          opengraphType
          schema {
            raw
          }
        }
      }
    }
  }
`

export default allInvestmentsTemplate
