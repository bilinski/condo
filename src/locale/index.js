import tranlations_en from "./en"
import tranlations_pl from "./pl"

const t = (name, lang = "pl") => {
  lang = lang.toLowerCase()
  try {
    switch (lang) {
      case "pl":
        return tranlations_pl[name] || name
      case "en":
        return tranlations_en[name] || name
      default:
        return name
    }
  } catch (error) {
    return name
  }
}
export default t
